package com.daniel.myapplication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.daniel.myapplication.R
import com.daniel.myapplication.databinding.SchoolListFragmentLayoutBinding
import com.daniel.myapplication.model.UIState
import com.daniel.myapplication.model.remote.SchoolNYCResponse
import com.daniel.myapplication.viewmodel.NYCViewModel

class SchoolFragment: Fragment() {
    private lateinit var binding: SchoolListFragmentLayoutBinding

    private val viewModel: NYCViewModel by viewModels()

    private val adapter: NYCAdapter by lazy {
        NYCAdapter {
            requireActivity().openDetails(it)
        }
    }

    private fun FragmentActivity.openDetails(dbn: String){
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, DetailFragment.newInstance(dbn))
            .addToBackStack(null)
            .commit()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        binding = SchoolListFragmentLayoutBinding.inflate(
            inflater,
            container,
            false
        )
        initViews()
        initObservables()
        return binding.root
    }

    private fun initObservables() {
        viewModel.data.observe(viewLifecycleOwner){
            when (it){
                is UIState.Response -> updateAdapter(it.data)
                is UIState.Error -> showError(it.errorMessage)
                else -> throw  Exception("Incorrect Fragment")
            }
        }
    }

    private fun showError(errorMessage: String) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun updateAdapter(data: List<SchoolNYCResponse>) {
        adapter.submitList(data)
    }

    private fun initViews() {
        binding.listSchools.layoutManager = LinearLayoutManager(context)
        binding.listSchools.adapter = adapter
    }
}