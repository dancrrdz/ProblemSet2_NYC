package com.daniel.myapplication.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.daniel.myapplication.databinding.SchoolDetailFragmentLayoutBinding
import com.daniel.myapplication.model.UIState
import com.daniel.myapplication.model.remote.DBN
import com.daniel.myapplication.model.remote.SchoolNYCSATScores
import com.daniel.myapplication.viewmodel.NYCViewModel

class DetailFragment: Fragment() {

    companion object{
        private const val TAG = "DetailFragment"
        fun newInstance(dbn: String) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(DBN, dbn)
                }
            }
    }

    private lateinit var binding: SchoolDetailFragmentLayoutBinding

    private val viewModel: NYCViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        binding = SchoolDetailFragmentLayoutBinding.inflate(
            inflater,
            container,
            false
        )
         arguments?.getString(DBN)?.let {
            requestDetails(it)
        }
        initObservables()
        return binding.root
    }

    private fun initObservables() {
        viewModel.data.observe(viewLifecycleOwner){
            when (it){
                is UIState.ResponseSat -> updateUI(it.data)
                is UIState.Error -> showError(it.errorMessage)
                else -> Log.d(TAG, "initObservables: ")
            }
        }
    }

    private fun updateUI(data: List<SchoolNYCSATScores>) {
        if (data.isNotEmpty())
            binding.apply {
                detailsHeader.text = data[0].dbn
                detailsTestAvg.text = data[0].sat_math_avg_score
                detailsTestReading.text = data[0].sat_critical_reading_avg_score
                detailsTestTakers.text = data[0].num_of_sat_test_takers
                detailsTestWriting.text = data[0].sat_writing_avg_score
            }
    }

    private fun showError(errorMessage: String) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun requestDetails(dbn: String) {
        viewModel.getDetails(dbn)
    }

}