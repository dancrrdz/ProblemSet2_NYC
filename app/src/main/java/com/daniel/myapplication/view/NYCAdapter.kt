package com.daniel.myapplication.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.daniel.myapplication.databinding.NycItemLayoutBinding
import com.daniel.myapplication.model.remote.SchoolNYCResponse

class NYCAdapter(private val openDetails: (String)-> Unit): ListAdapter<SchoolNYCResponse, NYCAdapter.NYCViewHolder>(UtilHelper) {

    class NYCViewHolder(private val binding: NycItemLayoutBinding): RecyclerView.ViewHolder(binding.root){

        fun onBind(item: SchoolNYCResponse, openDetails:(String)-> Unit){
            binding.schoolDetails.setOnClickListener{ openDetails(item.dbn) }
            binding.schoolDescription.text = item.school_sports
            binding.schoolName.text = item.school_name
        }
    }

    object UtilHelper: DiffUtil.ItemCallback<SchoolNYCResponse>(){
        override fun areItemsTheSame(
            oldItem: SchoolNYCResponse,
            newItem: SchoolNYCResponse
        ): Boolean {
            return oldItem.dbn == newItem.dbn
        }

        override fun areContentsTheSame(
            oldItem: SchoolNYCResponse,
            newItem: SchoolNYCResponse
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NYCViewHolder {
        return NYCViewHolder(
            NycItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NYCViewHolder, position: Int) {
        holder.onBind(currentList[position], openDetails)
    }

}
