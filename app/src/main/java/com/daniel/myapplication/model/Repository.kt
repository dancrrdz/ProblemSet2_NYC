package com.daniel.myapplication.model

import kotlinx.coroutines.flow.Flow

interface Repository {
    fun getListSchoolUseCase(): Flow<UIState>
    fun getListSatDetails(dbn: String): Flow<UIState>
}