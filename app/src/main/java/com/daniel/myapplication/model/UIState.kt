package com.daniel.myapplication.model

import com.daniel.myapplication.model.remote.SchoolNYCResponse
import com.daniel.myapplication.model.remote.SchoolNYCSATScores

sealed class UIState{
    data class Response(val data: List<SchoolNYCResponse>): UIState()
    data class ResponseSat(val data: List<SchoolNYCSATScores>): UIState()
    data class Error(val errorMessage: String): UIState()

}
