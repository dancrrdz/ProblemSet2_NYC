package com.daniel.myapplication.model.remote

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

//https://data.cityofnewyork.us/resource/s3k6-pzi2.json
// details
//https://data.cityofnewyork.us/resource/f9bf-2cp4.json

interface Service {
    @GET(ENDPOINT_SCHOOL)
    suspend fun getSchoolList(): Response<List<SchoolNYCResponse>>

    @GET(ENDPOINT_SAT)
    suspend fun getSchoolDetails(
        @Query(QUERY_DBN) dbn: String
    ): Response<List<SchoolNYCSATScores>>
}