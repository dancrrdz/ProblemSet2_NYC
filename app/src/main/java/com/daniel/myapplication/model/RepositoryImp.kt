package com.daniel.myapplication.model

import com.daniel.myapplication.model.remote.Network
import com.daniel.myapplication.model.remote.Service
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RepositoryImp: Repository {

    private val service: Service by lazy {
        Network.service
    }

    override fun getListSchoolUseCase(): Flow<UIState> {
        return flow {
            val response = service.getSchoolList()
            if (response.isSuccessful){
                response.body()?.let{
                    emit(UIState.Response(it))
                } ?: emit(UIState.Error(response.message()))
            } else{
                emit(UIState.Error(response.message()))
            }
        }
    }

    override fun getListSatDetails(dbn: String): Flow<UIState> {
        return flow {
            val response = service.getSchoolDetails(dbn)
            if (response.isSuccessful){
                response.body()?.let{
                    emit(UIState.ResponseSat(it))
                } ?: emit(UIState.Error(response.message()))
            } else{
                emit(UIState.Error(response.message()))
            }
        }
    }
}