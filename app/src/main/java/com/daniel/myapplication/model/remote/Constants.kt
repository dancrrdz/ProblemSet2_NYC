package com.daniel.myapplication.model.remote

const val ENDPOINT_SCHOOL="resource/s3k6-pzi2.json"
const val ENDPOINT_SAT = "resource/f9bf-2cp4.json"
const val BASE_URL = "https://data.cityofnewyork.us/"
const val QUERY_DBN = "dbn"
const val DBN = "DBN_Details"