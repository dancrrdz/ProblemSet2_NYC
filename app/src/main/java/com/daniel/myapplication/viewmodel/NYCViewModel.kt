package com.daniel.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daniel.myapplication.model.Repository
import com.daniel.myapplication.model.RepositoryImp
import com.daniel.myapplication.model.UIState
import kotlinx.coroutines.launch

class NYCViewModel: ViewModel() {

    private val repository: Repository by lazy{
        RepositoryImp()
    }

    private val _data = MutableLiveData<UIState>()
    val data: LiveData<UIState>
    get() = _data

    init {
        viewModelScope.launch {
            repository.getListSchoolUseCase().collect{
                _data.value = it
            }
        }
    }

    fun getDetails(dbn: String){
        viewModelScope.launch {
            repository.getListSatDetails(dbn).collect{
                _data.value = it
            }
        }
    }
}